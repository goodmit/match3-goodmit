﻿using UnityEngine;

public class Tiles : MonoBehaviour
{
    /* просто типы данных для ячеек на игровом поле
     * весьма просто в будущем расширить функционал игры и добавить
     * например фишку-бонус, которые будут помогать эффективнее зачищать уровни
     * или наоборот, сделают прохождение более сложным
     */
    public enum Type {
        MARKED = -2,
        LOCKED = -1,
        EMPTY = 0,
        BLUE = 1,
        GREEN = 2,
        PINK = 3,
        PURPLE = 4,
        RED = 5,
        WHITE = 6,
        YELLOW = 7,
        ORANGE = 8
    }

}
