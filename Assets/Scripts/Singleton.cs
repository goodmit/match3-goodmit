﻿using UnityEngine;

/// <summary>
/// Базовый класс для синглтонов
/// Создает экземпляр-одиночку в проекте.
/// </summary>
/// <typeparam name="T">Тип данных синглтона</typeparam>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T Instance => instance;

    protected static T instance;
    
    protected virtual void Awake() {
        if (instance == null) {
            instance = GetComponent<T>();
        }
        else {
            Debug.Log("Попытка создать больше одного экземпляра " + typeof(T) + "/n Объект будет удален!");
            Destroy(gameObject);
            
        }
    }

}

