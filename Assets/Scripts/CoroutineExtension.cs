﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* это класс-расширение для удобства работы с группами корутин
 * позволяет запустить одновремено группу корутин и ждать, когда она полностью отработает.
 */
public static class CoroutineExtension
{
    // для отслеживания используем словарь <название группы, количество работающих корутинов>
    static private readonly Dictionary<string, int> Runners = new Dictionary<string, int>();

    /// <summary>
    /// Регистрируем корутину в конкретную группу в конексте вызывающего класса.
    /// </summary>
    /// <param name="coroutine">Ссылка на регистрируемую корутину</param>
    /// <param name="parent">Предок - корутина, вызывающая зарегистрированне корутины</param>
    /// <param name="groupName">Идентификатор группы зарегистрированных корутин</param>
    public static void RegisterCoroutine(this IEnumerator coroutine, MonoBehaviour parent, string groupName)
    {
        if (!Runners.ContainsKey(groupName))
            Runners.Add(groupName, 0);

        Runners[groupName]++;
        parent.StartCoroutine(ParallelStarter(coroutine, parent, groupName));
    }

    // запускает параллельно каждую корутину в группе
    static IEnumerator ParallelStarter(IEnumerator coroutine, MonoBehaviour parent, string groupName)
    {
        yield return parent.StartCoroutine(coroutine);
        Runners[groupName]--;
    }

    /// <summary>
    /// Проверяем, отработала ли группа или еще нет
    /// </summary>
    /// <param name="groupName"></param>
    /// <returns></returns>
    public static bool IsGroupWorking(string groupName)
    {
        return (Runners.ContainsKey(groupName) && Runners[groupName] > 0);
    }
}
