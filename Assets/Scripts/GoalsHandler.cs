﻿using UnityEngine;

/// <summary>
/// Обрабатывает информацию о целях на игрвом уровне
/// </summary>
public class GoalsHandler : MonoBehaviour
{
    delegate void GoalDelegate();
    private GoalDelegate _delegate;

    private LevelRules.Goals goalType;
    private string _name;
    private float startTime;
    private bool _started;
    private int goalValue;
    private int _turns;
    private int _scores;

    public int timeRemain { get; private set; }
    
    // Update is called once per frame
    void Update()
    {
        if (!_started) return;
        UpdateTime();
        if (timeRemain <= 0 && !LevelController.Instance.blocked)
        {
            LevelController.Instance.GameOver();
        }
    }

    #region Public
    /// <summary>
    /// Инициализация хендлера
    /// </summary>
    /// <param name="goalType">Тип цели</param>
    /// <param name="goalValue">Значение цели</param>
    public void Init(LevelRules.Goals goalType, int goalValue)
    {
        this.goalType = goalType;
        this.goalValue = goalValue;

        switch (goalType)
        {
            case LevelRules.Goals.SCORES:
                _delegate = new GoalDelegate(ScoresGoal);
                break;
            case LevelRules.Goals.TIME:
                _delegate = new GoalDelegate(TimeGoal);
                break;
            case LevelRules.Goals.TURNS:
                _delegate = new GoalDelegate(TurnsGoal);
                break;
            default:
                _delegate = new GoalDelegate(TurnsGoal);
                break;
        }
    }

    void UpdateTime()
    {
        int newTime = goalValue - (int)(Time.time - startTime);
        timeRemain = newTime > 0 ? newTime : 0;
    }
    
    /// <summary>
    /// Запускаем хендлер
    /// </summary>
    public void Run()
    {
        _delegate.Invoke();
    }

    /// <summary>
    /// Стопаем хендлер
    /// </summary>
    public void Stop()
    {
        _started = false;
    }

    /// <summary>
    /// Обновление данных для хендлера
    /// </summary>
    /// <param name="turns">Количество сделанных игроком ходов</param>
    /// <param name="newScores">Набранные игроком очки</param>
    public void SetNewData(int turns, int newScores)
    {
        _turns = turns;
        _scores = newScores;
    }
    
    /// <summary>
    /// Название цели
    /// </summary>
    /// <returns></returns>
    public string Name()
    {
        return _name;
    }

    /// <summary>
    /// Отображает оставшееся время (для цели по времени)
    /// </summary>
    /// <returns></returns>
    public int GetTimeRemain()
    {
        return timeRemain;
    }

    /// <summary>
    /// Проверяет, достиг ли игрок цели уровня
    /// </summary>
    /// <returns></returns>
    public bool CheckGoals()
    {
        if (goalType == LevelRules.Goals.TIME)
        {
            return timeRemain <= 0;
        }
        else if (goalType == LevelRules.Goals.SCORES)
        {
            return _scores >= goalValue;
        }
        else
        {
            return goalValue <= _turns;
        }
        
    }

    /// <summary>
    /// Форматирует данные о хендлере в строковый вид
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        if(goalType == LevelRules.Goals.TIME)
        {
            return _name + timeRemain.ToString(); 
        } else if(goalType == LevelRules.Goals.SCORES)
        {
            return _name + _scores + "/" + goalValue;
        } else
        {
            return _name + (goalValue - _turns);
        }
    }
#endregion

    #region Private
    private void ScoresGoal()
    {
        _name = "Осталось набрать: ";
    }

    private void TurnsGoal()
    {
        _name = "Осталось ходов: ";
    }

    private void TimeGoal()
    {
        _name = "Осталось времени: ";
        startTime = Time.time;
        _started = true;
    }
    #endregion
}
