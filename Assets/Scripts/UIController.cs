﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Этот класс отвечает за UI элементы.
/// </summary>
/* фактически, это просто переключатель видимости парочки панелей
 * т.к. по тз большего и не требовалось */
public class UIController : MonoBehaviour
{
    [Header("Панели")]
    [Tooltip("Панель при проигрыше")]
    [SerializeField]
    private GameObject _gameOverPanel = default;
    [Tooltip("Панель очков")]
    [SerializeField]
    private GameObject _shuffleLabel = default;
    [Tooltip("Панель в игре")]
    [SerializeField]
    private GameObject _gamePanel = default;

    [Header("Интерфейс в игре")]
    [Tooltip("Отображает количество набранных очков")]
    [SerializeField]
    private Text _gameScoreTxt = default;
    [Tooltip("Отображает оставшееся количество ходов/очков/времени до конца игры")]
    [SerializeField]
    private Text _goalTxt = default;
    
    void Start()
    {
        ShowShufflePanel(false);
        _gameOverPanel.SetActive(false);
    }

    void FixedUpdate()
    {
        UpdateData();
    }
    
    /// <summary>
    /// обновляем данные для текстовых полей
    /// </summary>
    public void UpdateData()
    {
        _goalTxt.text = LevelController.Instance.GetHandler().ToString();
        _gameScoreTxt.text = "Очки: " + ScoreController.Instance.Score().ToString();
    }

    /// <summary>
    /// Показываем/прячем интерфейс в игре
    /// </summary>
    /// <param name="value"></param>
    public void ShowGamePanel(bool value = true)
    {
        _gamePanel.SetActive(value);
    }

    /// <summary>
    /// Показываем/прячем панель окончания игры
    /// </summary>
    /// <param name="value"></param>
    public void ShowGameOverPanel(bool value = true)
    {
        _gameOverPanel.SetActive(value);
    }

    /// <summary>
    /// Показываем/прячем окно с информацией о перетасовке
    /// </summary>
    /// <param name="value"></param>
    public void ShowShufflePanel(bool value = true)
    {
        _shuffleLabel.SetActive(value);
    }

}
