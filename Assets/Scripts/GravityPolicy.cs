﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Определяет "гравитацию" на уровне.
/// Задает направление падения фишек.
/// </summary>
public class GravityPolicy
{
    
    /* Этого не требовалось в ТЗ, но решил немножко добавить
     * вариантов на расширение функционала. помимо требуемого
     * варианта падения фишек снизу вверх, реализовал еще падение 
     * сверху вниз (остальные варианты просто присутствуют, но не реализованы).
     * варианты меняются в инспекторе. по идее 
     * можно даже переделать на автономный скрипт для каждой
     * отдельно взятой ячейки поля. чтобы разнообразить уровни.
     * чтобы направление падения фишек могло меняться в зависимости 
     * от зоны или в каждой ячейке поля по своему. */


    private LevelRules.Gravity _gravity;
    public Vector3 direction { get; private set; }
    public bool verticalOrientation { get; private set; }

    public GravityPolicy(LevelRules.Gravity gravity)
    {
        _gravity = gravity;
        verticalOrientation = true;
        Init();
    }

    private void Init()
    {
        switch(_gravity)
        {
            case LevelRules.Gravity.NONE:
                direction = Vector3.zero;
                break;
            case LevelRules.Gravity.TOP:
                direction = Vector3.up;
                break;
            case LevelRules.Gravity.RIGHT:
                verticalOrientation = false;
                direction = Vector3.right;
                break;
            case LevelRules.Gravity.BOTTOM:
                direction = Vector3.down;
                break;
            case LevelRules.Gravity.LEFT:
                verticalOrientation = false;
                direction = Vector3.left;
                break;
        }
        
    }

}
