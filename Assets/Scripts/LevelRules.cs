﻿using UnityEngine;

/// <summary>
/// Определяет базовые настройки для уровня
/// Фактически, просто цепляется к менеджеру уровня
/// и в инспекторе прописывюатся все значения.
/// </summary>
[System.Serializable]
public class LevelRules
{
    // количество ячеек-дырок на уровне
    [Range(0, 10)]
    public int blockedTiles = 3;

    // гравитация на уровне
    public Gravity gravity = Gravity.TOP;

    // количество цветов
    [Range(3, 8)]
    public int maxColors = 4;

    // тип цели уровня, при достижении которой игра завершится
    [Tooltip("Тип ограничения игровой сессии: количество ходов, набранные очки или время")]
    public Goals goal = Goals.TURNS;

    // значение цели уровня, при достижении которой игра завершится
    [Tooltip("Значение для ограничения игровой сессии")]
    public int goalValue;

    // размеры игрового поля
    [Header("Board Size")]
    public int rows = 6;
    public int columns = 6;

    public enum Gravity
    {
        NONE = -1,
        BOTTOM,
        LEFT,
        TOP,
        RIGHT
    }

    public enum Goals
    {
        SCORES,
        TURNS,
        TIME
    }

}
