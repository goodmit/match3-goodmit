﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Отвечает за перетасовку фишек на уровне
/// </summary>
public class Shuffler
{

    /// <summary>
    /// Перетасовка всех имеющихся на поле фишек
    /// </summary>
    /// <param name="baseTileTable">массив фишек</param>
    /// <param name="board">массив данных</param>
    public static void Shuffle(Tile[,] baseTileTable, ref int[,] board)
    {
        Tile[,] tiles = new Tile[baseTileTable.GetLength(0), baseTileTable.GetLength(1)];
        Array.Copy(baseTileTable, tiles, baseTileTable.Length);
        
        LevelController.Instance.SetBLock();
        
        List<Tile> line = new List<Tile>();

        CreateListForShuffle(tiles, line);

        // поставил 20 попыток на всякий случай чтобы не уйти в бесконечность
        // этот вариант очень маловероятен. но на тот редкий случай, когда такое произойдет
        // и с 20 попыток так и не удастся перетасовать поле, то можно например устроить Game Over
        // в данном тестовом задании это не будет реализовано. просто продолжим игру.
        int attempts = 20;
        while(!IsSuccesShuffle(tiles, ref board, line) && attempts > 0)
        {
            Array.Clear(tiles, 0, tiles.Length);
            Array.Copy(baseTileTable, tiles, baseTileTable.Length);
            CreateListForShuffle(tiles, line);
            attempts--;
        }
        Array.Clear(baseTileTable, 0, tiles.Length);
        Array.Copy(tiles, baseTileTable, baseTileTable.Length);

        LevelController.Instance.SetBLock(false);
    }

    /// <summary>
    /// Определяем, можно ли в указанную ячейку положить фишку с конкретным типом
    /// Проверка для Строк
    /// </summary>
    /// <param name="tiles">массив фишек</param>
    /// <param name="x">координата Х</param>
    /// <param name="y">координата У</param>
    /// <returns></returns>
    public static Tiles.Type GetDeniedTypeX(Tile[,]tiles, int x, int y)
    {
        Tiles.Type result = Tiles.Type.EMPTY;

        try
        {
            if (tiles[x - 1, y].type == tiles[x - 2, y].type)
            {
                result = tiles[x - 1, y].type;
            }
        }
        catch (IndexOutOfRangeException)
        {
            // за пределы массива нам вылезать нельзя
            // поэтому просто вернем пустой тип
            return result;
        }
        return result;
    }

    /// <summary>
    /// Определяем, можно ли в указанную ячейку положить фишку с конкретным типом
    /// Проверка для Столбцов
    /// </summary>
    /// <param name="tiles">массив фишек</param>
    /// <param name="x">координата Х</param>
    /// <param name="y">координата У</param>
    /// <returns></returns>
    public static Tiles.Type GetDeniedTypeY(Tile[,] tiles, int x, int y)
    {
        Tiles.Type result = Tiles.Type.EMPTY;
        try
        {
            if (tiles[x, y - 1].type == tiles[x, y - 2].type)
            {
                result = tiles[x, y - 1].type;
            }
        }
        catch (IndexOutOfRangeException)
        {
            // за пределы массива нам вылезать нельзя
            // поэтому просто вернем пустой тип
            return result;
        }
        
        return result;
    }

    // проверяем, успешно ли мы перетасовали фишки. не получилось ли после перемешивания готовых совпадений match3
    static bool IsSuccesShuffle(Tile[,] tiles, ref int[,] board, List<Tile> list)
    {
        for (int x = 0; x < tiles.GetLength(0); x++)
        {
            for (int y = 0; y < tiles.GetLength(1); y++)
            {
                if (tiles[x, y].type != Tiles.Type.LOCKED)
                {
                    Tiles.Type deniedX = Tiles.Type.EMPTY;
                    Tiles.Type deniedY = Tiles.Type.EMPTY;
                    deniedX = GetDeniedTypeX(tiles, x, y);
                    deniedY = GetDeniedTypeY(tiles, x, y);

                    int index = list.Count - 1;

                    while (index >= 0)
                    {
                        if (list[index].type != deniedX && list[index].type != deniedY) break;
                        index--;
                    }
                    
                    if (index < 0)
                    {
                        index = 0;
                        Debug.Log("Неразрешимая ситуация с перетасовкой! Пробуем заново зашафлить =(");
                        return false;
                    }

                    tiles[x, y] = list[index];
                    tiles[x, y].x = x;
                    tiles[x, y].y = y;
                    tiles[x, y].transform.position = new Vector3(x, y, 0);
                    tiles[x, y].transform.name = "Block[X:" + x + "Y:" + y + "]ID:" + tiles[x, y].type;
                    board[x, y] = (int)tiles[x, y].type;
                    list.RemoveAt(index);
                }
            }
        }
        return true;
    }

    // подготавливаем перетасованный список всех фишек 
    static void CreateListForShuffle(Tile[,] tiles, List<Tile> list)
    {
        list.Clear();
        foreach (Tile item in tiles)
        {
            if (item.type != Tiles.Type.LOCKED)
            {
                list.Add(item);

            }
        }
        ShuffleList(list);
    }

    // используем алгоритм перетасовки фишера - йетса
    // просто, но со вкусом
    static void ShuffleList(List<Tile> list)
    {
        for (int i = list.Count - 1; i >= 1; i--)
        {
            int j = UnityEngine.Random.Range(0, i + 1);
            Tile tmp = list[j];
            list[j] = list[i];
            list[i] = tmp;
        }
    }
    
}
