﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Управляет глобальными модулями приложения.
/// В основном, переключается межу сценами.
/// </summary>
public class GameController : Singleton<GameController>
{
    // загрузочный экран
    public Image fader;
    
    // флажок состояния смены сцены
    private bool isSceneLoading = false;

    // скорость изменения прозрачности загрузочного экрана
    public float fadeSpeed = .02f;
    // шаг увеличения/уменьшения альфы для загрузочного экрана
    private Color fadeTransparency = new Color(0, 0, 0, .04f);

    // текущая сцена
    private string currentScene;

    // отслеживает прогресс загрузки сцены
    private AsyncOperation async;

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }
    
    void Start()
    {
        fader.gameObject.SetActive(false);
        DontDestroyOnLoad(gameObject);
        //SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ReturnToMenu();
        }
        if(Input.GetKeyDown(KeyCode.Q))
        {
            LoadScene("Level_1");
        }
    }

    #region Public
    // запускаем процесс загрузки сцены по ее имени
    public void LoadScene(string sceneName)
    {
        if (isSceneLoading)
        {
            return;
        }

        StopAllCoroutines();
        instance.StartCoroutine(Load(sceneName));
        instance.StartCoroutine(FadeOut(fader));
        isSceneLoading = true;
    }

    // перезагружаем текущую сцену
    public void ReloadScene()
    {
        //StopAllCoroutines();
        LoadScene(SceneManager.GetActiveScene().name);
        //isSceneLoading = true;
    }
    
    /// <summary>
    /// разрешаем сменить сцену после ее загрузки
    /// </summary>
    public void ActivateScene()
    {
        async.allowSceneActivation = true;
    }

    /// <summary>
    /// получаем имя текущей сцены 
    /// </summary>
    public string CurrentSceneName
    {
        get { return currentScene; }
    }
    
    /// <summary>
    /// Возвращаемся в главное меню
    /// </summary>
    public void ReturnToMenu()
    {
        /*
        if (isSceneLoading)
        {
            return;
        }*/

        if (CurrentSceneName != "MainMenu")
        {
            //StopAllCoroutines();
            LoadScene("MainMenu");
            //isReturning = true;
        }
    }

    /// <summary>
    /// закрываем приложение
    /// </summary>
    public void QuitApp()
    {
    #if UNITY_STANDALONE
        Application.Quit();
    #endif
    #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
    #endif
    }

    #endregion

    #region Private
    // Плавно выводит из прозрачности заставку(в данном случае просто черный экран)
    IEnumerator FadeOut(Image fader)
    {
        fader.gameObject.SetActive(true);
        while (fader.color.a < 1)
        {
            fader.color += fadeTransparency;
            yield return new WaitForSeconds(fadeSpeed);
        }
        ActivateScene();
    }

    // Плавно убирает заставку(в данном случае просто черный экран) в прозрачность
    IEnumerator FadeIn(Image fader)
    {
        while (fader.color.a > 0)
        {
            fader.color -= fadeTransparency;
            yield return new WaitForSeconds(fadeSpeed);
        }
        fader.gameObject.SetActive(false);
    }

    // Begin loading a scene with a specified string asynchronously
    IEnumerator Load(string sceneName)
    {
        async = SceneManager.LoadSceneAsync(sceneName);
        async.allowSceneActivation = false;
        yield return async;
        isSceneLoading = false;
    }

    private void OnLoadLevel(string name)
    {
        LoadScene(name);
    }

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        currentScene = scene.name;
        instance.StartCoroutine(FadeIn(fader));
    }
    #endregion

}
