﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Контроллер игрового уровня. 
/// Управляет различными модулями игрового уровня.
/// </summary>
[RequireComponent(typeof(GoalsHandler))]
public class LevelController : Singleton<LevelController>
{

    //
    [SerializeField]
    private BoardManager level = default;

    [Header("Настройки для уровня")]
    [SerializeField]
    private LevelRules rules = default;
    [Tooltip("Обработчик ограничения игровой сессии")]
    
    private GoalsHandler goalsHandler;
    private UIController UIManager;

    public bool blocked { get; private set; }

    public int width { get; private set; }
    public int height { get; private set; }

    private int _turns = 0;

    private List<int> collectLines;

    // Start is called before the first frame update
    void Start()
    {
        UIManager = GetComponent<UIController>();
        SetBLock();
        level.Init(rules);
        width = rules.rows;
        height = rules.columns;
        goalsHandler = gameObject.GetComponent<GoalsHandler>();
        goalsHandler.Init(rules.goal, rules.goalValue);
        goalsHandler.SetNewData(_turns, ScoreController.Instance.Score());
        Camera.main.transform.position = new Vector3(width / 2, height / 2, -3);
    }

    /// <summary>
    /// Запускаем обработчик игровых целей для уровня
    /// </summary>
    public void ReadyToStart()
    {
        goalsHandler.Run();
    }

    /// <summary>
    /// Запускаем механизм обработки хода игрока
    /// </summary>
    /// <returns></returns>
    public IEnumerator HandlePlayerTurn()
    {
        SetBLock();
        yield return StartCoroutine(level.Swap());
        
        StopCoroutine(level.Swap());
        collectLines = level.GetAllMatchLines();
        if (collectLines.Count > 0)
        {
            SetBLock();
            UpdateScore();
            level.DestroyAllMarked();
            level.ShiftTiles();
        }
        else
        {
            SetBLock();
            yield return StartCoroutine(level.Swap());
            SetBLock(false);
        }

        Tile.targetTile = null;
        Tile.destinationTile = null;
        StopCoroutine(level.Swap());
    }

    /// <summary>
    /// Проверяем на количество совпадений уже после обновления поля
    /// </summary>
    public void CheckMatches()
    {
        SetBLock();
        collectLines = level.GetAllMatchLines();
        if (collectLines.Count > 0)
        {
            UpdateScore();
            level.DestroyAllMarked();
            level.ShiftTiles();
        }
        else
        {
            _turns++;
            
            goalsHandler.SetNewData(_turns, ScoreController.Instance.Score());
            if (goalsHandler.CheckGoals())
            {
                GameOver();
                print("Игра закончена!");
                return;
            }
            CheckPossibles(true);
            SetBLock(false);
        }
    }

    /// <summary>
    /// Блокировка игрового поля.
    /// Если поле блокировано, игрок не сможет двигать фишки
    /// </summary>
    /// <param name="value"></param>
    public void SetBLock(bool value = true)
    {
        blocked = value;
    }

    /// <summary>
    /// Проверка на доступные ходы
    /// </summary>
    public void CheckPossibles(bool showInfo = false)
    {
        //print("проверяем наличие возможных ходов");
        bool success = false;
        for (int i = 0; i < 3; i++)
        {
            if (level.HasPossibleTurns())
            {
                success = true;
                return;
            }
            if (showInfo)
            {
                StartCoroutine(ShowInfoMsg());
            }
            //print("готовим тасование!");
            Shuffler.Shuffle(level.allTiles, ref level.board);
        }

        /* если алгоритм перемешивания по каким-то неведомым причинам 
         * не смог перетасовать фишки, чтобы не было готовых совпадений
         * и чтобы оставались еще доступны ходы, значит завершаем игру
         * */
        if(!success)
        {
            GameOver();
        }

    }

    /// <summary>
    /// Завершшение игры
    /// </summary>
    public void GameOver()
    {
        SetBLock();
        UIManager.ShowGameOverPanel();
    }

    /// <summary>
    /// Возврат в главное меню
    /// </summary>
    public void ToMenu()
    {
        GameController.Instance.ReturnToMenu();
    }

    /// <summary>
    /// Возвращаем обработчик целей игрового уровня
    /// </summary>
    /// <returns></returns>
    public GoalsHandler GetHandler()
    {
        return goalsHandler;
    }

    /// <summary>
    /// Возвращает значение цели уровня
    /// </summary>
    /// <returns></returns>
    public int GetGoalStatus()
    {
        return rules.goalValue;
    }

    /// <summary>
    /// Перезапускаем уровень
    /// </summary>
    public void RestartLevel()
    {
        GameController.Instance.ReloadScene();
    }

    /// <summary>
    /// Обновляем количество набранных очков
    /// </summary>
    void UpdateScore()
    {
        foreach (int line in collectLines)
        {
            ScoreController.instance.AddScore(line);
        }
        collectLines.Clear();
    }

    // показываем сообщение что нет доступных ходов
    IEnumerator ShowInfoMsg()
    {
        SetBLock();
        UIManager.ShowShufflePanel();
        yield return new WaitForSeconds(1);
        UIManager.ShowShufflePanel(false);
        SetBLock(false);
    }

}
