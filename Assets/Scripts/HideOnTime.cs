﻿using UnityEngine;

public class HideOnTime : MonoBehaviour
{

    [SerializeField]
    private float lifeTime = 1;
    private float startTime;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (lifeTime < Time.time - startTime) KillMe();
    }

    void KillMe()
    {
        gameObject.SetActive(false);
    }

}
