﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Описывает игровую фишку
/// </summary>
public class Tile : MonoBehaviour
{
    public static Transform targetTile;
    public static Transform destinationTile;

    //позиция фишки в таблице
    public int x;
    public int y;

    // тип фишки
    public Tiles.Type type;

    // помечена для удаления
    public bool isMarked;
    // свапается ли сейчас
    public bool isSwapping { get; private set; }
    // находится ли в движении сейчас
    public bool isDropping { get; private set; }

    private Vector3 baseScale;
    private GameObject selectedIcon;
    
    private void Awake()
    {
        baseScale = transform.localScale;
    }

    void Start()
    {
        isSwapping = false;
        isDropping = false;
        isMarked = false;
        
        selectedIcon = transform.GetComponentInChildren<SpriteRenderer>().gameObject;
        selectedIcon.SetActive(false);
    }
    
    void OnMouseOver()
    {
        if (type == Tiles.Type.LOCKED) return;
        // курсор мыши над блоком
        transform.localScale = new Vector3(baseScale.x + 0.2f, baseScale.y + 0.2f, baseScale.z + 0.2f);

        // если уровень в блокировке, то больше ничего не предпринимаем
        if (LevelController.Instance.blocked) return;

        //выделение объектов
        if (Input.GetMouseButtonDown(0))
        {
            //если ни один блок не выделен, то выделяем нажатый блок
            if (!targetTile)
            {
                SetMarkedTile();
            }
            else
            {
                targetTile.GetComponent<Tile>().selectedIcon.SetActive(false);
                
                // если нажали на уже выделенный блок, то снимаем выделение с него
                if (targetTile == transform)
                {
                    targetTile = null;
                    return;
                }
                
                // если нажали на любой другой блок, то проверям, находятся ли оба блока рядышком или нет.
                if (AreNeighbours())
                {
                    destinationTile = transform;
                    LevelController.Instance.SetBLock();
                    StartCoroutine( LevelController.Instance.HandlePlayerTurn());
                }
                else
                {
                    SetMarkedTile();
                }
            }
        }
    }

    void OnMouseExit()
    {
        if (type == Tiles.Type.LOCKED) return;
        //сбрасываем скейл блока
        transform.localScale = baseScale;
    }

    #region Public
    /// <summary>
    /// Свапаем фишку с другой
    /// </summary>
    public void Swap()
    {
        StartCoroutine(Swapping());
    }

    /// <summary>
    /// Помечаем фишку для взаимодействия с ней
    /// </summary>
    public void SetMarkedTile()
    {
        targetTile = transform;
        selectedIcon.SetActive(true);
    }

    /// <summary>
    /// Меняем значение состояния, находится ли сейчас фишка в движении
    /// </summary>
    /// <param name="value"></param>
    public void SetDropping(bool value = true)
    {
        isDropping = value;
    }

    /// <summary>
    /// Заствляет переместить фишку на сцене в нужные координаты
    /// </summary>
    /// <param name="id">ид для процесса перемещения конкретной фишки</param>
    /// <returns></returns>
    public IEnumerator Dropping(int id)
    {
        SetDropping();
        Vector3 newPos = new Vector3(x, y, 0);
        while (IsWeightyDistance(transform.position - newPos, 0.08f))
        {
            if(transform.position.x >= -1 &&
               transform.position.y >= -1 &&
               transform.position.x <= LevelController.Instance.width &&
               transform.position.y <= LevelController.Instance.height)
            {
                transform.localScale = Vector3.one;
            }
            transform.Translate((newPos - transform.position) * 0.25f);
            yield return new WaitForFixedUpdate();
        }
        transform.position = new Vector3(x, y, 0);
        //print("Фишка приземлилась! id:" + id);
        SetDropping(false);
    }

    #endregion

    #region Private

    // корутина для свапа фишки
    IEnumerator Swapping()
    {
        isSwapping = true;
        Vector3 newPos = new Vector3(x, y, 0);
        while (IsWeightyDistance(transform.position - newPos))
        {
            //transform.position = Vector3.Lerp(transform.position, new Vector3(x, y, 0), 12 * Time.deltaTime);
            transform.Translate((newPos - transform.position) * 0.2f);
            yield return new WaitForFixedUpdate();
        }
        transform.position = new Vector3(x, y, 0);
        isSwapping = false;
        //print("Фишку переместили!");
    }

    // проверяем, достаточно близко ли подъехала к цели фишка
    bool IsWeightyDistance(Vector3 distance, float deflection = 0.01f)
    {
        return Mathf.Abs(distance.x) >= deflection || Mathf.Abs(distance.y) >= deflection;
    }

    // проверка, является ли фишка соседней по отношению к другой фишке
    bool AreNeighbours()
    {
        Tile firstTile = targetTile.gameObject.GetComponent<Tile>();
        Tile secondTile = transform.gameObject.GetComponent<Tile>();

        //Проверка рядом ли находится другая фишка
        if (firstTile.x - 1 == secondTile.x && firstTile.y == secondTile.y)
        {
            //слева
            return true;
        }
        if (firstTile.x + 1 == secondTile.x && firstTile.y == secondTile.y)
        {
            //справа
            return true;
        }
        if (firstTile.x == secondTile.x && firstTile.y + 1 == secondTile.y)
        {
            //сверху
            return true;
        }
        if (firstTile.x == secondTile.x && firstTile.y - 1 == secondTile.y)
        {
            //снизу
            return true;
        }
        Debug.Log("Эти фишки нельзя свапать! Они слишком далеко друг от друга!");
        return false;
    }
    #endregion

}
