﻿using UnityEngine;

/// <summary>
/// Контроллер игрового счета
/// Отвечает за логику начисления/хранения игровых очков, набранных игроком
/// </summary>
public class ScoreController : Singleton<ScoreController>
{
     
    private int _score;
    private int _hiscore;

    // Start is called before the first frame update
    void Start()
    {
        _score = 0;
        LoadData();
        ResetScore();
    }

    #region Public
    /// <summary>
    /// Добавляет игровые очки
    /// </summary>
    /// <param name="value">Длина собранной линии</param>
    public void AddScore(int value)
    {
        // формулу вписал сюда для сокращения сроков выполнения тестового задания.
        // по хорошему эти вещи хорошо бы выносить в конфиги. для адекватных правок баланса гдшниками
        int result = 10 + 5 * (value - 3);

        _score += result;
    }

    /// <summary>
    /// Обновляет статистику по игровым очкам и рекордам
    /// </summary>
    public void GameOver()
    {
        if(_score > _hiscore)
        {
            _hiscore = _score;
            SaveHiscore();
        }
    }

    /// <summary>
    /// Возвращает текущее количество очков
    /// </summary>
    /// <returns></returns>
    public int Score()
    {
        return _score;
    }

    /// <summary>
    /// Возвращает рекордное количество набранных очков
    /// </summary>
    /// <returns></returns>
    public int Hiscore()
    {
        return _hiscore;
    }

    /// <summary>
    /// Сбрасывает текущие очки до нуля
    /// </summary>
    public void ResetScore()
    {
        _score = 0;
    }

    /* Хотя в данном ТЗ и не требовалось, но решил добавить
     * возможность сохранения максимального счета в PlayerPrefs 
     */
    /// <summary>
    /// Сохраняет лучший счет
    /// </summary>
    public void SaveHiscore()
    {
        PlayerPrefs.SetInt("hiscore", _hiscore);
    }
    #endregion

    #region Private
    private void LoadData()
    {
        _hiscore = PlayerPrefs.GetInt("hiscore", 0);
    }
    #endregion
}
